package com.example.goaweek1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {

    //Declaring some of the variables
    double totalInt;
    boolean firstTime = true;
    boolean isADecimal = false;
    DecimalFormat decimalFormat = new DecimalFormat("#.####################");
    String operator = "";
    String subTotalValue;
    String firstValueString = "0";
    String secondValueString = "0";
    String thirdValueString = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Assigning all the buttons to a variable
        Button dot = findViewById(R.id.button51);
        Button zero = findViewById(R.id.button52);
        Button one = findViewById(R.id.button41);
        Button two = findViewById(R.id.button42);
        Button three = findViewById(R.id.button43);
        Button four = findViewById(R.id.button31);
        Button five = findViewById(R.id.button32);
        Button six = findViewById(R.id.button33);
        Button seven = findViewById(R.id.button21);
        Button eight = findViewById(R.id.button22);
        Button nine = findViewById(R.id.button23);
        Button plus = findViewById(R.id.button44);
        Button multiply = findViewById(R.id.button24);
        Button divide = findViewById(R.id.button13);
        Button minus = findViewById(R.id.button34);
        Button equals = findViewById(R.id.button53);
        Button change = findViewById(R.id.button12);
        Button allClear = findViewById(R.id.button11);

        //Giving all the buttons a onclicklisener
        dot.setOnClickListener(this);
        zero.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        plus.setOnClickListener(this);
        multiply.setOnClickListener(this);
        divide.setOnClickListener(this);
        minus.setOnClickListener(this);
        equals.setOnClickListener(this);
        change.setOnClickListener(this);
        allClear.setOnClickListener(this);

    }

    @Override
    //This method is called when you press any button
    public void onClick(View v) {
        //Assigning the total textview and subtotal texview
        TextView total = (TextView) findViewById(R.id.Total);
        TextView subTotal = (TextView) findViewById(R.id.subTotal);
        //Using a switch case to find out which button is pressed
        switch (v.getId()) {
            case R.id.button51:
                if(!isADecimal && !total.getText().toString().isEmpty()) {
                    isADecimal = true;
                    total.setText(total.getText()+".");
                }
                break;
            case R.id.button52:
                total.setText(total.getText()+"0");
                break;
            case R.id.button41:
                total.setText(total.getText()+"1");
                break;
            case R.id.button42:
                total.setText(total.getText()+"2");
                break;
            case R.id.button43:
                total.setText(total.getText()+"3");
                break;
            case R.id.button31:
                total.setText(total.getText()+"4");
                break;
            case R.id.button32:
                total.setText(total.getText()+"5");
                break;
            case R.id.button33:
                total.setText(total.getText()+"6");
                break;
            case R.id.button21:
                total.setText(total.getText()+"7");
                break;
            case R.id.button22:
                total.setText(total.getText()+"8");
                break;
            case R.id.button23:
                total.setText(total.getText()+"9");
                break;
            case R.id.button12:
                //Change button
                //Checking if total is empty
                if(!total.getText().toString().isEmpty()) {
                    //Checking if total is not negative if so making it negative
                        total.setText("-" + total.getText().toString());
                        //Checking if total is negative if so making it positive
                    } else if (total.getText().toString().contains("-")) {
                        total.setText(total.getText().toString().substring(1));
                    }
                break;
            case R.id.button11:
                //All clear button
                //Resets everything
                total.setText("");
                total.setHint("0");
                subTotal.setText("");
                operator = "";
                firstTime = true;
                isADecimal = false;
                total.setTextSize(64);
                total.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                break;
            case R.id.button44:
                //Plus button
                if(!total.getText().toString().isEmpty()) {//Making sure it's not empty
                    firstValueString = total.getText().toString();//Assigning firstValueString
                    operator = "+";//Making the operate "+" so it adds when we press equals
                    subTotal.setText(total.getText() + " + ");//Setting the subtotal and adding +
                    total.setHint(total.getText().toString());//Setting the hint
                    subTotalValue = subTotal.getText().toString();//Assigning subTotalValue so we can change the subtotal later
                    total.setText("");//Resetting the text
                    firstTime = true;//Since it's a new calculation it would be the first time
                    isADecimal = false;//Since there is a new number it will no longer be a decimal
                }
                break;
            case R.id.button24:
                //Multiply button
                //Almost everything else is the same as plus button
                if(!total.getText().toString().isEmpty()) {
                    firstValueString = total.getText().toString();
                    operator = "*";
                    subTotal.setText(total.getText() + " * ");
                    total.setHint(total.getText().toString());
                    subTotalValue = subTotal.getText().toString();
                    total.setText("");
                    firstTime = true;
                    isADecimal = false;
                }
                break;
            case R.id.button13:
                //Divide button
                //Almost everything else is the same as plus button
                if(!total.getText().toString().isEmpty()) {
                    firstValueString = total.getText().toString();
                    operator = "/";
                    subTotal.setText(total.getText() + " / ");
                    total.setHint(total.getText().toString());
                    subTotalValue = subTotal.getText().toString();
                    total.setText("");
                    firstTime = true;
                    isADecimal = false;
                }
                break;
            case R.id.button34:
                //Subtract button
                //Almost everything else is the same as plus button
                if(!total.getText().toString().isEmpty()) {
                    firstValueString = total.getText().toString();
                    operator = "-";
                    subTotal.setText(total.getText() + " - ");
                    total.setHint(total.getText().toString());
                    subTotalValue = subTotal.getText().toString();
                    total.setText("");
                    firstTime = true;
                    isADecimal = false;
                }
                break;
            case R.id.button53:
                //Equals button
                secondValueString = total.getText().toString();
                if (secondValueString.isEmpty()) {
                    secondValueString = "0";
                }
                if (firstTime){
                    firstTime = false;
                    if (subTotal.getText().toString().isEmpty()){
                        subTotal.setText(secondValueString + " =");
                    }else {
                        subTotal.setText(subTotalValue + secondValueString + " =");
                    }
                    thirdValueString = secondValueString;
                    double firstValueDouble = Double.parseDouble(firstValueString);
                    double secondValueDouble = Double.parseDouble(secondValueString);
                    if(operator == "+"){
                        totalInt = firstValueDouble + secondValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if(operator == "*"){
                        totalInt = firstValueDouble * secondValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if(operator == "-"){
                        totalInt = firstValueDouble - secondValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if (operator == "/"){
                        totalInt = firstValueDouble / secondValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else{
                        total.setText("");
                        total.setHint(decimalFormat.format(secondValueDouble));
                        if(total.getText().toString().contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }
                }else {
                    double secondValueDouble = Double.parseDouble(secondValueString);
                    double thirdValueDouble = Double.parseDouble(thirdValueString);
                    if(operator == "+"){
                        subTotal.setText(secondValueString + " + " + thirdValueString + " =");
                        totalInt = secondValueDouble + thirdValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if(operator == "*"){
                        subTotal.setText(secondValueString + " * " + thirdValueString + " =");
                        totalInt = secondValueDouble * thirdValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if(operator == "-"){
                        subTotal.setText(secondValueString + " - " + thirdValueString + " =");
                        totalInt = secondValueDouble - thirdValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else if (operator == "/"){
                        subTotal.setText(secondValueString + " / " + thirdValueString + " =");
                        totalInt = secondValueDouble / thirdValueDouble;
                        String totalString = (decimalFormat.format(totalInt));
                        total.setText(totalString);
                        if(totalString.contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }else{
                        total.setText("");
                        total.setHint(decimalFormat.format(thirdValueDouble));
                        if(total.getText().toString().contains(".")){
                            isADecimal = true;
                        }else{
                            isADecimal = false;
                        }
                    }
                }

                break;
        }

    }

}